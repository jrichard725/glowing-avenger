package src.jr.integritymobile.infiniteregistry;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

@SuppressLint("NewApi")
public class NewDevice extends Activity implements OnClickListener {

	private Button signInBtn;
	private int uid_val;
	private ProgressDialog progress;
	public final static String EXTRA_USER = "jr.integritymobile.infiniteregistry.USER";
	public final static String EXTRA_UID = "jr.integritymobile.infiniteregistry.UID";
	public final static String EXTRA_MESSAGE = "jr.integritymobile.infiniteregistry.MESSAGE";


	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_device);

		// Check for network status - if no - alert user
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			signInBtn = (Button) findViewById(R.id.signIn);

			signInBtn.setOnClickListener(this);
			// Show the Up button in the action bar.
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				setupActionBar();
			}
		} else {
			new Helper().alert("The Infinite Registry requires an internet/data connection");
		}
	}

	@Override
	public void onClick(View v) {
		// respond to clicks
		if (v.getId() == R.id.signIn) {
			Context context = (Context) this;
			progress = new ProgressDialog(context);
			EditText userRaw = (EditText) findViewById(R.id.username);
			EditText passwordRaw = (EditText) findViewById(R.id.password);
			String username = userRaw.getText().toString();
			String password = passwordRaw.getText().toString();

			String myurl = buildURL(TextUtils.htmlEncode(username), TextUtils.htmlEncode(password));

			try {
				new Helper().execute(myurl);
			} catch (Exception e) {
			}

		}
	}

	private String buildURL(String username, String password) {
		String url = "http://www.theinfiniteregistry.com/password.php?user="
				+ username + "&pass=" + password;
		return url;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_device, menu);
		return true;
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	private void redirect(int uid) {
		boolean existing;
		EditText userRaw = (EditText) findViewById(R.id.username);
		String username = userRaw.getText().toString();

		if (uid_val == 0) {
			existing = false;
		} else {
			existing = true;
		}

		if (!existing) {
			Intent intentNew = new Intent(this, NewUser.class);
			intentNew.putExtra(EXTRA_USER, username);
			startActivity(intentNew);
		} else {
			Intent intentMain = new Intent(this, MainActivity.class);
			intentMain.putExtra(EXTRA_USER, username);
			intentMain.putExtra(EXTRA_UID, uid_val + "");
			startActivity(intentMain);
		}

	}

	private class Helper extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setTitle("Looking up user information...");
			progress.setMessage("Searching...");
			progress.setCancelable(false);
			progress.setIndeterminate(true);
			progress.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			try {
				return get(urls[0]);
			} catch (IOException e) {
				return "Unable to retrieve web page. URL may be invalid.";
			}
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			progress.dismiss();
			uid_val = Integer.parseInt(result);
			redirect(uid_val);
		}

		protected void alert(String msg) {
			Toast toast = Toast.makeText(getApplicationContext(), msg,
					Toast.LENGTH_SHORT);
			toast.show();
		}

		@SuppressWarnings("rawtypes")
		private String get(String myurl) throws IOException {
			String error_msg = "", json = "";
			InputStream is = null;
			// Only display the first len characters of the retrieved
			// web page content.
			int len = 500;

			try {
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);

				// Starts the query
				conn.connect();
				is = conn.getInputStream();

				// Convert the InputStream into a string
				json = (String) readIt(is, len);

				Gson gson = new Gson();
				jsonType uid = gson.fromJson(json, jsonType.class);

				conn.disconnect();

				if (is != null) {
					is.close();
				}

				return uid.uid;
			} catch (Exception e) {
				Class type = e.getClass();
				error_msg = "Error Type: " + type.getName() + "\nMessage: "
						+ e.getLocalizedMessage() + "\nCause:" + e.getCause();
				return error_msg;
				// Makes sure that the InputStream is closed after the app is
				// finished using it.
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

		// Reads an InputStream and converts it to a String.
		public String readIt(InputStream stream, int len) throws IOException,
				UnsupportedEncodingException {
			Reader reader = null;
			reader = new InputStreamReader(stream, "UTF-8");
			char[] buffer = new char[len];
			reader.read(buffer);
			String values = new String(buffer);

			int startIndex = values.indexOf("{");
			int endIndex = values.indexOf("}");
			values = values.substring(startIndex, endIndex + 1);

			return (String) values;
		}

		private class jsonType {
			public String uid;
		}
	}
}
