package src.jr.integritymobile.infiniteregistry;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ScanResult extends Activity implements OnClickListener {

	private Button submitBtn, more, less;
	private TextView qty;
	private String uid_val, scanUPC, listid, itemid;
	private int quantity = 1;
	private ProgressDialog progress;
	public final static String EXTRA_UPC = "jr.integritymobile.infiniteregistry.UPC";
	public final static String EXTRA_CON = "jr.integritymobile.infiniteregistry.CON";
	public final static String EXTRA_FORMAT = "jr.integritymobile.infiniteregistry.FORM";
	public final static String EXTRA_LISTID = "jr.integritymobile.infiniteregistry.LISTID";
	public final static String ENCODING = "UTF-8";
	public final static String DEBUG_TAG = "infiniteregistry.DEBUG";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_result);

		// Check for network status - if no - alert user
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			setContentView(R.layout.activity_scan_result);

			Typeface font = Typeface.createFromAsset(getAssets(),
					"ambassador_script.ttf");

			submitBtn = (Button) findViewById(R.id.submit_btn);
			more = (Button) findViewById(R.id.more);
			less = (Button) findViewById(R.id.less);

			submitBtn.setTypeface(font);
			more.setTypeface(font);
			less.setTypeface(font);

			qty = (TextView) findViewById(R.id.item_qty);
			qty.setText(quantity + "");

			submitBtn.setOnClickListener(this);
			more.setOnClickListener(this);
			less.setOnClickListener(this);
			// Show the Up button in the action bar.
			setupActionBar();

			Intent intentStart = getIntent();
			scanUPC = intentStart.getStringExtra(Items.EXTRA_CON);
			listid = intentStart.getStringExtra(Items.EXTRA_LISTID);
			String scanFormat = intentStart.getStringExtra(Items.EXTRA_FORMAT);
			String scanType = intentStart.getStringExtra(Items.EXTRA_UPC);

			if (scanUPC != null && scanFormat != null && scanType != null) {
				// we have a result
				Context context = (Context) this;
				progress = new ProgressDialog(context);
				
				String url = buildURL(scanUPC);
				try {
					new Helper().execute(url);
				} catch (Exception e) {}
			} else {
				Toast toast = Toast.makeText(getApplicationContext(),
						"No scan data received!", Toast.LENGTH_SHORT);
				toast.show();
			}
			// Show the Up button in the action bar.
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				setupActionBar();
			}
		} else {
			new Helper()
					.alert("The Infinite Registry requires an internet/data connection");
		}
	}
	
	public void updateView(String json){
		if(json == "" || json == null || json.indexOf("{") < 0){
			Intent intent = new Intent(this, Items.class);
			intent.putExtra(EXTRA_LISTID, listid);
			startActivity(intent);
		}else{
			TableLayout layout = (TableLayout) findViewById(R.id.res_lay);
			setContentView(layout);
			
			int startIndex = json.indexOf("{");
			int endIndex = json.indexOf("}");
			
			String element = json.substring(startIndex, endIndex + 1);
			TableRow tr = new TableRow(this);
			int keyInd = element.indexOf("\":")+2;
			int commaInd = element.indexOf(",");
			itemid = element.substring(keyInd, commaInd);
			
			keyInd = element.indexOf("\":", commaInd)+2;
			commaInd = element.indexOf(",", keyInd);
	
			String prod = element.substring(keyInd);
			TextView text = new TextView(this); 
			text.setTextSize(40);
			text.setText(prod);
			text.setTextSize(35);
			
			tr.addView(text);
			
			layout.addView(tr);
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.submit_btn) {
			String myurl = buildURLSubmit(listid, itemid, (String) qty.getText());

			try {
				new Helper().execute(myurl);
			} catch (Exception e) {}
			
		} else if (v.getId() == R.id.more) {
			quantity++;
		} else if (v.getId() == R.id.less) {
			if (quantity > 0)
				quantity--;
			else
				quantity = 0;
		}
		qty.setText(quantity + "");
		// respond to clicks
	}

	private String buildURL(String upc) {
		try {
			upc = URLEncoder.encode(upc, ENCODING);
		} catch (Exception e) {}
		String url = "http://www.theinfiniteregistry.com/search.php?type=scan&upc="+ upc;
		Log.d(DEBUG_TAG, "URL: " + url);
		return url;
	}

	private String buildURLSubmit(String listid, String itemid, String qty) {
		try {
			listid = URLEncoder.encode(listid, ENCODING);
			itemid = URLEncoder.encode(itemid, ENCODING);
			qty = URLEncoder.encode(qty, ENCODING);
		} catch (Exception e) {}
		String url = "http://www.theinfiniteregistry.com/insert.php?type=scan&listid="+ listid+"&itemid="+itemid+"&qty="+qty;
		Log.d(DEBUG_TAG, "URL: " + url);
		return url;
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.scan_result, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class Helper extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setTitle("Looking up item...");
			progress.setMessage("Searching...");
			progress.setCancelable(false);
			progress.setIndeterminate(true);
			progress.show();
		}

		@Override
		protected String doInBackground(String... urls) {

			// params comes from the execute() call: params[0] is the url.
			try {
				return get(urls[0]);
			} catch (IOException e) {
				return "Unable to retrieve web page. URL may be invalid.";
			}
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			progress.dismiss();
			uid_val = result;
			updateView(uid_val);
		}

		protected void alert(String msg) {
			Toast toast = Toast.makeText(getApplicationContext(), msg,
					Toast.LENGTH_SHORT);
			toast.show();
		}

		@SuppressWarnings("rawtypes")
		private String get(String myurl) throws IOException {
			String error_msg = "", json = "";
			InputStream is = null;
			// Only display the first len characters of the retrieved
			// web page content.
			int len = 500;

			try {
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);

				// Starts the query
				conn.connect();
				is = conn.getInputStream();

				// Convert the InputStream into a string
				json = (String) readIt(is, len);


				return json;
			} catch (Exception e) {
				Class type = e.getClass();
				error_msg = "Error Type: " + type.getName() + "\nMessage: "
						+ e.getLocalizedMessage() + "\nCause:" + e.getCause();
				return error_msg;
				// Makes sure that the InputStream is closed after the app is
				// finished using it.
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

		// Reads an InputStream and converts it to a String.
		public String readIt(InputStream stream, int len) throws IOException,
				UnsupportedEncodingException {
			Reader reader = null;
			reader = new InputStreamReader(stream, "UTF-8");
			char[] buffer = new char[len];
			reader.read(buffer);
			String values = new String(buffer);

			// Limit the result to only the json
			int startIndex = values.indexOf("{");
			int endIndex = values.indexOf("}");
			values = values.substring(startIndex, endIndex + 1);

			return (String) values;
		}
	}

}
