package src.jr.integritymobile.infiniteregistry;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

@SuppressLint("NewApi")
public class List extends Activity implements OnClickListener{

	private ProgressDialog progress;
	private String uid;
	public final static String EXTRA_LISTID = "jr.integritymobile.infiniteregistry.LISTID";
	public final static String EXTRA_UID = "jr.integritymobile.infiniteregistry.UID";
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		
		Intent intentStart = getIntent();
		uid = intentStart.getStringExtra(MainActivity.EXTRA_UID);
		if(uid == "" || uid == null)
			uid = intentStart.getStringExtra(Create.EXTRA_UID);
		
		Context context = (Context) this;
		progress = new ProgressDialog(context);
		
		String myurl = buildURL(uid);
		try {
			new Helper().execute(myurl);
		} catch (Exception e) {}
		
		// Make sure we're running on Honeycomb or higher to use ActionBar APIs
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
	}
	
	private String buildURL(String uid) {
		String url ="";
		try{
			uid = URLEncoder.encode(uid, "UTF-8");
		} catch (Exception e){}
		
		url = "http://www.theinfiniteregistry.com/search.php?type=list&uid="+ uid;
		return url;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		String list_id = ""+id;		
		Intent intent = new Intent(this, Items.class);
		intent.putExtra(EXTRA_LISTID, list_id);
		intent.putExtra(EXTRA_UID, uid);
		startActivity(intent);
	}
	
	public void updateView(String json){
		TableLayout layout = (TableLayout) findViewById(R.id.list_lay);
		setContentView(layout);		
		Helper help = new Helper();
		int startIndex = json.indexOf("{");
		int endIndex = json.indexOf("}");
		while(help.hasNext(json, "}", endIndex)){
			String element = json.substring(startIndex, endIndex + 1);
			int keyInd = json.indexOf("\":")+2;
			int commaInd = json.indexOf(",");
			String listid = element.substring(keyInd, commaInd);
			keyInd = json.indexOf("\":", commaInd)+2;
			String listname = element.substring(keyInd, endIndex);
			TableRow tr = new TableRow(this);
			TextView text = new TextView(this); 
			text.setTextSize(40);
			text.setText(listname);
			int textId = Integer.parseInt(listid);
			text.setId(textId);
			text.setClickable(true);
			text.setOnClickListener(this);
			text.setTextSize(35);
			tr.addView(text);
			layout.addView(tr);
			if(endIndex+2 < json.length()){
				json = json.substring(endIndex+2);
				startIndex = json.indexOf("{");
				endIndex = json.indexOf("}");
			}else{
				json = "";
				endIndex=0;
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.list, menu);
		return true;
	}

	private class Helper extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setTitle("Looking up registries...");
			progress.setMessage("Searching...");
			progress.setCancelable(false);
			progress.setIndeterminate(true);
			progress.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			try {
				return get(urls[0]);
			} catch (IOException e) {
				return "Unable to retrieve web page. URL may be invalid.";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			progress.dismiss();
			updateView(result);
		}

		@SuppressWarnings("rawtypes")
		private String get(String myurl) throws IOException {
			String error_msg = "", json = "";
			InputStream is = null;
			// Only display the first len characters of the retrieved
			// web page content.
			int len = 500;

			try {
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				conn.connect();
				is = conn.getInputStream();
				json = (String) readIt(is, len);
				conn.disconnect();

				if (is != null) {
					is.close();
				}				
					
				return json;
			} catch (Exception e) {
				Class type = e.getClass();
				error_msg = "Error Type: " + type.getName() + "\nMessage: "	+ e.getLocalizedMessage() + "\nCause:" + e.getCause();
				return error_msg;
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

		// Reads an InputStream and converts it to a String.
		public String readIt(InputStream stream, int len) throws IOException,
				UnsupportedEncodingException {
			Reader reader = null;
			reader = new InputStreamReader(stream, "UTF-8");
			char[] buffer = new char[len];
			reader.read(buffer);
			String values = new String(buffer);

			// Limit the result to only the json
			int startIndex = values.indexOf("{");
			int endIndex = values.indexOf("}");
			while(hasNext(values, "}", endIndex+1))
				endIndex = values.indexOf("}", endIndex+1);
			values = values.substring(startIndex, endIndex + 1);
			return (String) values;
		}
		
		public boolean hasNext(String haystack, String needle, int start){
			boolean found = false;
			if(haystack.indexOf(needle, start) > -1){
				found = true;
			}
			
			return found;
		}
	}
}
