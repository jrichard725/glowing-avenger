package src.jr.integritymobile.infiniteregistry;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class NewUser extends Activity implements OnClickListener {

	private Button submitBtn;
	private EditText usernameTxt;
	private TextView error;
	private String uid_val;
	private ProgressDialog progress;
	public final static String EXTRA_USER = "jr.integritymobile.infiniteregistry.USER";
	public final static String EXTRA_UID = "jr.integritymobile.infiniteregistry.UID";
	public final static String DEBUG_TAG = "infiniteregistry.DEBUG";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Check for network status - if no - alert user
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			String user = "";
			setContentView(R.layout.activity_new_user);
			Intent intentSent = getIntent();

			Typeface font = Typeface.createFromAsset(getAssets(),
					"ambassador_script.ttf");

			submitBtn = (Button) findViewById(R.id.submit);
			usernameTxt = (EditText) findViewById(R.id.user);

			submitBtn.setTypeface(font);

			if (intentSent.getStringExtra(NewDevice.EXTRA_USER) instanceof String) {
				user = intentSent.getStringExtra(NewDevice.EXTRA_USER);
				usernameTxt.setText(user);
			}

			submitBtn.setOnClickListener(this);
			// Show the Up button in the action bar.
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				setupActionBar();
			}
		} else {
			new Helper()
					.alert("The Infinite Registry requires an internet/data connection");
		}
	}

	@Override
	public void onClick(View v) {
		// respond to clicks
		Context context = (Context) this;
		progress = new ProgressDialog(context);
		error = (TextView) findViewById(R.id.error);
		EditText userRaw = (EditText) findViewById(R.id.user);
		EditText passwordRaw = (EditText) findViewById(R.id.pass);
		EditText passwordConfRaw = (EditText) findViewById(R.id.passConf);
		EditText emailRaw = (EditText) findViewById(R.id.email);
		String username = userRaw.getText().toString();
		String password = passwordRaw.getText().toString();
		String passwordConf = passwordConfRaw.getText().toString();
		String email = emailRaw.getText().toString();

		String myurl = buildURL(TextUtils.htmlEncode(username), TextUtils.htmlEncode(password), TextUtils.htmlEncode(email));

		if (!password.equals(passwordConf)) {
			error.setText("Passwords did not match please try again.");
		} else {
			try {
				new Helper().execute(myurl);
			} catch (Exception e) {
			}
		}
	}

	private String buildURL(String username, String password, String email) {
		String url = "http://www.theinfiniteregistry.com/insert.php?type=user&user="
				+ username + "&pass=" + password + "&email=" + email;
		Log.d(DEBUG_TAG, "URL: " + url);
		return url;
	}

	private void redirect(String uid_val) {
		Log.d(DEBUG_TAG, "Returned value: " + uid_val);
		if (uid_val.indexOf("username") > -1 || uid_val.indexOf("email") > -1) {
			error.setText(uid_val);
		} else {
			Intent intentInfo = getIntent();
			String username = intentInfo.getStringExtra(NewDevice.EXTRA_USER);

			if (intentInfo.getStringExtra(NewDevice.EXTRA_USER) instanceof String) {
				Intent intentMain = new Intent(this, MainActivity.class);
				intentMain.putExtra(EXTRA_USER, username);
				intentMain.putExtra(EXTRA_UID, uid_val);
				startActivity(intentMain);
			}
		}

	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_user, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class Helper extends AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setTitle("Creating new user...");
			progress.setMessage("Creating...");
			progress.setCancelable(false);
			progress.setIndeterminate(true);
			progress.show();
		}

		@Override
		protected String doInBackground(String... urls) {

			// params comes from the execute() call: params[0] is the url.
			try {
				return get(urls[0]);
			} catch (IOException e) {
				return "Unable to retrieve web page. URL may be invalid.";
			}
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			progress.dismiss();
			uid_val = result;
			redirect(uid_val);
		}

		protected void alert(String msg) {
			Toast toast = Toast.makeText(getApplicationContext(), msg,
					Toast.LENGTH_SHORT);
			toast.show();
		}

		@SuppressWarnings("rawtypes")
		private String get(String myurl) throws IOException {
			String error_msg = "", json = "";
			InputStream is = null;
			// Only display the first len characters of the retrieved
			// web page content.
			int len = 500;

			try {
				URL url = new URL(myurl);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(10000 /* milliseconds */);
				conn.setConnectTimeout(15000 /* milliseconds */);
				conn.setRequestMethod("GET");
				conn.setDoInput(true);

				// Starts the query
				conn.connect();
				is = conn.getInputStream();

				// Convert the InputStream into a string
				json = (String) readIt(is, len);

				if (json.indexOf("error") > -1) {
					String error = "";
					if (json.indexOf("email") > -1) {
						error = "email address";
					} else if (json.indexOf("username") > -1) {
						error = "username";
					}
					return "That " + error
							+ " already exists. Please try again.";
				} else {
					Gson gson = new Gson();
					jsonType id = gson.fromJson(json, jsonType.class);
					conn.disconnect();

					if (is != null) {
						is.close();
					}

					return id.uid;
				}
			} catch (Exception e) {
				Class type = e.getClass();
				error_msg = "Error Type: " + type.getName() + "\nMessage: "
						+ e.getLocalizedMessage() + "\nCause:" + e.getCause();
				return error_msg;
				// Makes sure that the InputStream is closed after the app is
				// finished using it.
			} finally {
				if (is != null) {
					is.close();
				}
			}
		}

		// Reads an InputStream and converts it to a String.
		public String readIt(InputStream stream, int len) throws IOException,
				UnsupportedEncodingException {
			Reader reader = null;
			reader = new InputStreamReader(stream, "UTF-8");
			char[] buffer = new char[len];
			reader.read(buffer);
			String values = new String(buffer);

			// Limit the result to only the json
			int startIndex = values.indexOf("{");
			int endIndex = values.indexOf("}");
			values = values.substring(startIndex, endIndex + 1);

			return (String) values;
		}

		// This class is used to set the variables needed from the gson fromjson
		// method
		public class jsonType {
			public String uid;
		}
	}

}
