package src.jr.integritymobile.infiniteregistry;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;

import android.util.Log;

public class Utility {
    protected String EXTRA_UPC, EXTRA_CON, EXTRA_FORMAT, EXTRA_LISTID, DEBUG_TAG;

	public Utility(){
		//Set constants that can be referenced by object
		this.EXTRA_UPC = "jr.integritymobile.infiniteregistry.UPC";
		this.EXTRA_CON = "jr.integritymobile.infiniteregistry.CON";
		this.EXTRA_FORMAT = "jr.integritymobile.infiniteregistry.FORM";
		this.EXTRA_LISTID = "jr.integritymobile.infiniteregistry.LISTID";
		this.DEBUG_TAG = "infiniteregistry.DEBUG";
	}

	
    /*
     *
     *   This function accepts a url as a string and will return a JSONObject
     *		or null if it fails
    */
	public JSONArray get_json(String myurl){
        String json_string = "";
        JSONArray json = null;
        InputStream is = null;

        // Only display the first len characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            is = conn.getInputStream();
            json_string = (String) readIt(is, len);
            conn.disconnect();

            if (is != null) {
                is.close();
            }				

			json = new JSONArray(json_string);
        } catch (Exception e) {
            return null;
        } finally {
            if (is != null) {
                try {
					is.close();
				} catch (IOException e) {
					Log.d(this.DEBUG_TAG, "Failed to close Input Stream");
				}
            }
        }

        return json;
    }

    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream, int len) throws IOException,
            UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        String values = new String(buffer);

        return values;
    }
}
